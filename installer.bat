@echo off
echo off
::edit below to latest version of node 
set nodever=v14.13.1

::variables
::folder name of node 
set nodefile=node-%nodever%-win-x64
:: zip node is downloaded as 
set nodezip=%nodefile%.zip
::path to node installation (path willl be added to user paths)
set nodepath =C:\apps\%nodefile%
:: path to node in downloads
set downloadpath =%userprofile%\downloads\%nodezip%


echo starting setup for node %nodever%
IF EXIST "%downloadPath%" GOTO found
start chrome https://nodejs.org/dist/%nodever%/%nodezip%

:downloading
IF EXIST "%downloadPath%" GOTO downloaded
timeout /t 1
echo waiting for download to finish
goto downloading
echo download completed
GOTO downloaded

:found 
echo %nodezip% has been found

:downloaded
IF EXIST "C:\apps\nodefile" GOTO extract
mkdir C:\apps\

:extract
echo extracting 
tar.exe -xf %downloadPath% -C C:\apps\
echo extraction complete

:path
echo setting up path variables
FOR /F "tokens=2*" %%A IN ('REG.EXE QUERY "HKCU\Environment" /v "PATH"') DO SET my_user_path=%%B
echo.%my_user_path% | findstr /C:"%nodepath%" 1>nul
if errorlevel 1 (
GOTO setpath
) ELSE (
echo path to %nodefile% is already present
GOTO angularinstall
)

:setpath
setx my_user_path "%my_user_path%"
setx PATH "%nodepath%;%my_user_path%"
@echo off

:angularinstall
echo installing angular
set PATH=%nodepath%;%PATH%
npm install -g @angular/cli

pause
::optional steps
@ECHO OFF
SET tidy=
SET /P tidy=press y to delete node install files
IF "%tidy%"=="y" GOTO tidying
GOTO End

:tidying
echo tidying up 
@echo off
del /s /Q %downloadPath%
echo tidying up completed

:End
echo installation complete
echo enjoy the workshop :)
pause